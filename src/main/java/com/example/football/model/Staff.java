package com.example.football.model;

//import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
//import org.hibernate.annotations.OnDelete;
//import org.hibernate.annotations.OnDeleteAction;

@Entity
@Table(name = "staffs")
public class Staff  {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonSerialize
    private Long id;

    @NotNull
    @Lob
    private String type;
    
    @NotNull
    @Lob
    private String description;
    
    

    @ManyToOne(fetch = FetchType.LAZY)
 //   @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "equipe_id", nullable = false)
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) 
    
//    @OnDelete(action = OnDeleteAction.CASCADE)
//    @JsonIgnore
    private Equipe equipe;

    // Getters and Setters
    
    public String getType() {
        return this.type;
    }
 
    public void setType(String type) {
        this.type = type;
    }
    
    public String getDescription() {
        return this.description;
    }
 
    public void setDescription(String desc) {
        this.description = desc;
    }
    
    public Equipe getEquipe() {
        return this.equipe;
    }
    
    public void setEquipe(Equipe equipe) {
        this.equipe = equipe;
    }
}