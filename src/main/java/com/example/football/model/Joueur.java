package com.example.football.model;

//import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
//import org.hibernate.annotations.OnDelete;
//import org.hibernate.annotations.OnDeleteAction;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Entity
@Table(name = "joueurs")
public class Joueur  {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonSerialize
  //  @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @NotNull
    @Lob
    private String nom;
    
    @NotNull
    @Lob
    private String prenom;
    
    @NotNull
    @Lob
    private String position;
    

    @ManyToOne(fetch = FetchType.LAZY)
 //   @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "equipe_id", nullable = false)
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) 
 //   @OnDelete(action = OnDeleteAction.CASCADE)
 //   @JsonIgnore
    private Equipe equipe;

    
    
    
    // Getters and Setters
    
    public String getNom() {
        return this.nom;
    }
 
    public void setNom(String nom) {
        this.nom = nom;
    }
    
    public String getPrenom() {
        return this.prenom;
    }
 
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }
    
    public String getPosition() {
        return this.position;
    }
 
    public void setPosition(String position) {
        this.position = position;
    }
    
    public Equipe getEquipe() {
        return this.equipe;
    }
    
    public void setEquipe(Equipe equipe) {
        this.equipe = equipe;
    }
    
    
}