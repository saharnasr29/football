package com.example.football.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.*;

@Entity
@Table(name = "equipes")
public class Equipe  {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonSerialize
    private Long id;
    

    @NotNull
    @Size(max = 100)
    @Column(unique = true)
    private String pays;

    @NotNull
    @Size(max = 2500)
    private String description;

    
    @OneToMany(cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,
            mappedBy = "equipe")
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private List<Staff> staffs = new ArrayList<>();
    
    

    
    
    @OneToMany(cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,
            mappedBy = "equipe")
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private List<Joueur> joueurs = new ArrayList<>();

    // Getters and Setters 
    
    public String getPays() {
        return this.pays;
    }
 
    public void setPays(String pays) {
        this.pays = pays;
    }
    
    public String getDescription() {
        return this.description;
    }
 
    public void setDescription(String desc) {
        this.description = desc;
    }
}
