package com.example.football.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.football.model.User;

@Repository
public interface UserDao extends CrudRepository<User, Integer> {
	User findByUsername(String username);
}