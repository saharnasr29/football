package com.example.football.repository;

import com.example.football.model.Joueur;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface JoueurRepository extends JpaRepository<Joueur, Long> {
    Page<Joueur> findByEquipeId(Long equipeId, Pageable pageable);
    Optional<Joueur> findByIdAndEquipeId(Long id, Long equipeId);
}