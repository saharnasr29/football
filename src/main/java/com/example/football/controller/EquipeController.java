package com.example.football.controller;

import com.example.football.exception.ResourceNotFoundException;
import com.example.football.model.Equipe;
import com.example.football.repository.EquipeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

import javax.validation.Valid;


@RestController
public class EquipeController {

    @Autowired
    private EquipeRepository equipeRepository;

 //   @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/equipes")
    public Page<Equipe> getAllEquipes(Pageable pageable) {
        return equipeRepository.findAll(pageable);
    }
    

    @GetMapping("equipe/{equipeId}")
    public Optional<Equipe> getEquipeId(@PathVariable Long equipeId)
    {
     return equipeRepository.findById(equipeId);
    }
    

    @PostMapping("/equipes")
    public Equipe createEquipe(@Valid @RequestBody Equipe equipe) {
        return equipeRepository.save(equipe);
    }

    @PutMapping("/equipes/{equipeId}")
    public Equipe updateEquipe(@PathVariable Long equipeId, @Valid @RequestBody Equipe equipeRequest) {
        return equipeRepository.findById(equipeId).map(equipe -> {
            equipe.setPays(equipeRequest.getPays());
            equipe.setDescription(equipeRequest.getDescription());
        
            return equipeRepository.save(equipe);
        }).orElseThrow(() -> new ResourceNotFoundException("EquipeId " + equipeId + " not found"));
    }


    @DeleteMapping("/equipes/{equipeId}")
    public ResponseEntity<?> deleteEquipe(@PathVariable Long equipeId) {
        return equipeRepository.findById(equipeId).map(equipe -> {
            equipeRepository.delete(equipe);
            return ResponseEntity.ok().build();
        }).orElseThrow(() -> new ResourceNotFoundException("EquipeId " + equipeId + " not found"));
    }

}